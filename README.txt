PCBWay link
https://www.pcbway.com/project/member/?bmbno=9025CDCF-CE39-41

Gitlab link
https://gitlab.com/IObrizio

Twitter
https://twitter.com/IObrizio

Instagram
https://www.instagram.com/iobrizio

Tiktok
https://www.tiktok.com/@iobrizio

Instructables
https://www.instructables.com/member/IObrizio/




/*************************************************************/

LM317 minimum load to maintain proper regulation = typ 3.5mA & max = 10mA

VO is calculated as shown in Equation 1. IADJ is typically 50 μA and negligible in most applications.
VO = VREF (1 + R2 / R1) + (IADJ × R2)

R2 potentiomerter covers the full range with a value of 5k
